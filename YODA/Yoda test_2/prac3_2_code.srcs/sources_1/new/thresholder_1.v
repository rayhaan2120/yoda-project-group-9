`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.05.2022 09:29:04
// Design Name: 
// Module Name: thresholder_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module thresholder_1(
    input clk,
    input [15:0] Threshold,
    output Write,
    output [15:0] Out,
    input [15:0] Mean,
    output [31:0] Addr
    );
    
    reg [7:0] counter;
    reg [15:0] Min;
    reg [15:0] Max;
    initial begin
     //Intiialise values
        Min = Mean - Threshold ;
        Max = Mean + Threshold ;
        counter = 0;
    end
    
    always @(posedge clk) begin
    //If greater than min or max then filter out
        write = 0;
        if (Sample < Max) begin
            if (Sample > Min) begin
                Write = 1;
                Output  = Sample;       
                end
            end 
        end  
endmodule
