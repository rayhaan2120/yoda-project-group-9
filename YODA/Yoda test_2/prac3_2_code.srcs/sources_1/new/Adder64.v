`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.05.2022 19:10:12
// Design Name: 
// Module Name: Adder64
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Adder64(
    input clk,
    input [63:0] Sample1,
    input [63:0] Sample2,
    output [63:0] Output
    );
    
    
    always@(Sample1) begin
    Output =  Sample1 + Sample2; //Add the samples with no memory 
    end
endmodule
