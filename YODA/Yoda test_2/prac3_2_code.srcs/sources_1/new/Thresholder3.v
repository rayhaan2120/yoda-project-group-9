`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.05.2022 18:09:37
// Design Name: 
// Module Name: Thresholder3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Thresholder3(
    input clk,
    input [15:0] Threshold,
    input [15:0] Mean,
    input [15:0] In,
    output [3:0] WriteEb,
    output [15:0] Out,
    output [31:0] Addr,
    output ReadEn,
    input Ready
    );
     
    reg [7:0] counter; //We're assuming interval = 255 samples
    reg [15:0] Min;
    reg [15:0] Max;
    integer interval;
    initial begin
        //Initialise values to 0 
        counter = 0;
        counter2 = 0;
        Addr = 32'h0;
        WriteEb = 0;
         counter = 0;
        
        //Initialise Threshold values 
        Min = Mean - Threshold ;
        Max = Mean + Threshold ;
       
    end
    
    always @(posedge clk) begin   
        WriteEb = 0; //Write enable
        
        if (Ready) begin //Check if all samples are ready
            Addr = Addr + counter; //Set address
            ReadEn =1; //Read enable
           
           // If the sample lies between the min and max values then write to output memory 
           // Else set the value in that address to 0
            if (In < Max) begin
                if (In > Min) begin
                    Out = In;       
                    end 
                    else begin
                        Out = 0;
                    end
                end 
                else begin
                    Out = 0 ;                 
                end
             WriteEb = 1;
            counter = counter +1; //Increment memory address
        end
        
    end  
endmodule
