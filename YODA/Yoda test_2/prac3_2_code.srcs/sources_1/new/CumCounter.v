`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.05.2022 16:25:10
// Design Name: 
// Module Name: CumCounter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CumCounter(
    input clk,
    input reg [15:0] Sample,
    output reg [15:0] Output
    );
    
    reg [7:0] InnerCounter;
    initial begin
        Output = 0;
        
    end
    
    always@(Sample) begin
        if (InnerCounter==254) begin
            Output = Output + 2; //Increase the number of samples by 2 for min and Max 
        end
        InnerCounter = InnerCounter +1;
    end
    
    
endmodule
