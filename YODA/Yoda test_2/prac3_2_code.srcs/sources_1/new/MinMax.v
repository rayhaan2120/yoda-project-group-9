`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.05.2022 11:33:28
// Design Name: 
// Module Name: MinMax
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MinMax(
    input clk,
    input [15:0] Sample,
    input [7:0] Interval,
    output reg [15:0] Min,
    output reg [15:0] Max
    );
    
    reg [7:0] counter;
    initial begin
        //Intiialise min value to highest possible value and Max value to 0
        Min = 16'hffff;
        Max = 0;
        counter = 0;
    end
    
    always @(posedge clk) begin
       
        //If a sample if greater than the current max value then update the max 
        if (Sample > Max) begin
            Max  = Sample;
        end
        
         //If a sample if less than the current min value then update the min  
        if (Sample < Min) begin
            Min  = Sample;
        end
        counter = counter + 1; //Increment the counter for the interval managemebt 
        
        if (counter > Interval) begin //Interval management 
           
           // Reset values
           counter = 0;
           Min = 16'hffff;
           Max = 0;
        end
        
    end   
 
endmodule
