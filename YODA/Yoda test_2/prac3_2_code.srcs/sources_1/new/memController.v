`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Donkey Kong HQ
// Engineer: Jimmy Neutron 
// 
// Create Date: 22.05.2022 17:40:05
// Design Name: 
// Module Name: memController
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module memController(
    input clk,
    input [15:0] Sample,
    output [31:0] Addr,
    output [3:0] Write,
    output [15:0] Output
    );
    
     
    reg [7:0] counter; //We're assuming interval = 255 samples 
    reg [15:0] numIntervals; //We're assuming the numnber of intevals is known
    integer interval;
    initial begin
        counter = 0;
        counter2 = 0;
        Addr = 'h0;
        Write = 0;
        numIntervals = 118;
    end
    
    always @(posedge clk) begin    
    //Only write values to memory at the end of each interval
        if (counter > Interval) begin
           
           //Only write values to memory if  
           if (counter2<numIntervals) begin
                addr=addr +counter2;
                counter2 = counter2 +1;
                Output = Sample;
                Write = 1;
           end
           else begin
           counter2 = numIntervals  + 1;
           Write = 0;
           end      
        end 
        else begin 
            Write = 0;
            counter = counter + 1;
        end
    end    
    
endmodule
