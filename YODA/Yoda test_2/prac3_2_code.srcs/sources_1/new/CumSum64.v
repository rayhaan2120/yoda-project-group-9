`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.05.2022 15:22:01
// Design Name: 
// Module Name: CumSum64
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CumSum64(
    input clk, 
    input [31:0] Sample,
    output [63:0] Output
    );
    
    initial begin
        // Initialise output to 0
        Output = 0;
    end
    
    always@(Sample) begin
    Output = Output + Sample; // Accumulate Samples
    end
endmodule
