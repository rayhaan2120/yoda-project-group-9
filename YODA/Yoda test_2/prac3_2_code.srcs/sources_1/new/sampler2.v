`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.05.2022 18:27:34
// Design Name: 
// Module Name: sampler2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sampler2(
    input clk,
    output reg Reddy,
    output reg [16:0] Out
    );
    
    parameter SIZE = 30000;    
reg [15:0] rom_memory [SIZE-1:0];
integer i;

initial begin
    $readmemh("yoda.mem", rom_memory); //Use IP of BRAM instead of this command
    i = 0;
    Reddy = 0;
end
    
//At every positive edge of the clock, output a sine wave sample.
always@(posedge clk) begin
    if (i <SIZE) begin
    Out = rom_memory[i];
    i = i+1; 
    end
    else begin
        Reddy = 1;
    end
  
end
endmodule
