`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.05.2022 20:39:07
// Design Name: 
// Module Name: shifter_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module shifter_1(
    input clk,
    input [15:0] sineInput,
    input [15:0] amp,
    input [15:0] shift,
    output reg [31:0] sineOutput
    );
parameter SIZE = 1024;    
reg [15:0] rom_memory [SIZE-1:0];
integer i;

initial begin
    $readmemh("sine_LUT_values.mem", rom_memory); //Use IP of BRAM instead of this command
    i =SIZE*shift/360.00 - 1; 
   
end
    
//At every positive edge of the clock, output a sine wave sample.
always@(posedge clk) begin
    sineOutput = amp*(rom_memory[i]);
    i = i+ 1;
    if((i)== SIZE) 
        i = 0;      
end
endmodule
