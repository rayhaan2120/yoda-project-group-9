`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.05.2022 11:51:50
// Design Name: 
// Module Name: CumSum_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


//Comments similar to other cumsum iterations

module CumSum_1(
    input clk,
    input [15:0] Sample,
    output reg [31:0] Output
    );
    
    initial begin
        
        Output = 0;
    end
    
    always@(Sample) begin
    Output = Output + Sample;
    end
endmodule
