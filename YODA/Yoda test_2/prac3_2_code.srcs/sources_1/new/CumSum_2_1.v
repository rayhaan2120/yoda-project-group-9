`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.05.2022 18:55:14
// Design Name: 
// Module Name: CumSum_2_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//Comments similar to other cumsum iterations

module CumSum_2_1(
    input clk,
    input [15:0] Sample1,
    input [15:0] Sample2,
    output [31:0] Output
    );
     initial begin
        // Initialise output to 0
        Output = 0;
    end
    
    always@(Sample1) begin
    Output = Output + Sample1 + Sample2; // Accumulate Samples
    end
    
endmodule
