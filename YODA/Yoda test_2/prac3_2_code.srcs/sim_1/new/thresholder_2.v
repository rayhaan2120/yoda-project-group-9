`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.05.2022 09:36:48
// Design Name: 
// Module Name: thresholder_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module thresholder_2(
    input clk,
    input [15:0] Sample,
    input [15:0] Threshold,
    input [15:0] Mean,
    output reg [15:0] Out,
    output reg Write,
    output reg [31:0] Addr
    );
    
    reg [7:0] counter;
    reg [15:0] Min;
    reg [15:0] Max;
    initial begin
        Min = Mean - Threshold ;
        Max = Mean + Threshold ;
        counter = 0;
    end
    
    always @(posedge clk) begin
        Write = 0;
        if (Sample < Max) begin
            if (Sample > Min) begin
                Write = 1;  
                Out  = Sample;       
                end
            end 
        end  
    
    
endmodule
