`timescale 1ns / 1ps

module testBench;
    reg clk;
    wire [15:0] sine;
    wire [31:0] sineS;
    reg [15:0] amp_reg = 2;
    reg [15:0] shift_reg = 90;
    reg [7:0] inti = 254;
    wire [15:0] minout;
    wire [15:0] maxout;
    
    
    wire [15:0] threshout;
    reg [15:0] Mean = 5306;
    reg [15:0] threshVal = 100;
    wire  write;
    wire  [31:0] Address;
    
    
    
    //initates and connects the sine generator to the testBench
    sine_gen baseSineGen(
        .clk (clk),
        .sineOutput (sine)
    );
    
    MinMax minimax (
        .clk(clk),
        .Sample(sine),
        .Interval(inti),
        .Min(minout),
        .Max(maxout)
    );
    
    thresholder_2 threshy(
        .clk(clk),
        .Sample(maxout),
        .Write(write),
        .Out(threshout),
        .Mean(Mean),
        .Addr(Address),
        .Threshold(threshVal)
    
    );
    
    
    
    //frequency control
    parameter freq = 100000000.00; //100 MHz
    parameter SIZE = 1024.00; 
    parameter clockRate = 1000000000000.00/(freq*2.0*SIZE) ; //clock time (make this an output from the sine modules)
    parameter amplification = 2;
    parameter phase_shift = 90;
 
    //Generate a clock with the above frequency control
    initial
    begin 
    
    
    
    clk = 1'b0;
    end
    always #clockRate clk = ~clk; //#1 is one nano second delay (#x controlls the speed)
    
endmodule
